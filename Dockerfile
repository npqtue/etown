
FROM node:6.9.2

WORKDIR /app
ADD package.json /app/package.json
RUN npm install

ADD . /app

ENV NODE_ENV production
ENV DEBUG false

EXPOSE 4444
CMD npm start
