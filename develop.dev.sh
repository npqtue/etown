#!/usr/bin/env bash

DOCKER_COMPOSE="docker-compose -p etown-realtime-dev -f docker-compose.dev.yml"


${DOCKER_COMPOSE} build
${DOCKER_COMPOSE} down || true
${DOCKER_COMPOSE} up
