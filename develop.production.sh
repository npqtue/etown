#!/usr/bin/env bash

DOCKER_COMPOSE="docker-compose -p etown-realtime-production -f docker-compose.production.yml"

${DOCKER_COMPOSE} build
${DOCKER_COMPOSE} kill
${DOCKER_COMPOSE} rm -f
${DOCKER_COMPOSE} up -d
