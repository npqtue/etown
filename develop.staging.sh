#!/usr/bin/env bash

DOCKER_COMPOSE="docker-compose -p etown-realtime-staging -f docker-compose.staging.yml"

${DOCKER_COMPOSE} build
${DOCKER_COMPOSE} kill
${DOCKER_COMPOSE} rm -f
${DOCKER_COMPOSE} up -d
