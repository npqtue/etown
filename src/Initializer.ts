import ConversationManager from './service/ConversationRedis'

export default function Initializer() {
    return ConversationManager.initializer()
}
