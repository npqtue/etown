const app = require('express')()
const server = require('http').Server(app)
const io = require('socket.io')(server, {pingInterval: 4000, pingTimeout: 5000})
const redisAdapter = require('socket.io-redis')
import Logger from './service/Logger'
import {SafeParseJSON} from './service/StringUtil'
import * as request from 'request'
import {Convention, userAction, USER} from './service/Constants'
import Redis from "./service/RedisBase"
import ConversationManager from './service/ConversationRedis'

export default class Realtime {
    public static privateIO: any
    public static publicIO: any
    private io: any
    private port: number

    constructor(port: number = Number(process.env.PORT)) {
        this.io = io
        this.io.set('transports', ['websocket', 'flashsocket', 'htmlfile', 'xhr-polling', 'jsonp-polling', 'polling'])
        this.io.adapter(redisAdapter({
            host: process.env.REDIS_HOST,
            port: process.env.REDIS_PORT
        }))
        Realtime.privateIO = io.of('/private')
        Realtime.publicIO = io.of('/public')
        Realtime.privateIO.use(this.validateAuthenticate)
        this.port = port
        this.listen()
    }

    public send(event: string, data: any) {
        return Realtime.publicIO.emit(event, JSON.stringify(data))
    }

    public sendToSocket(socketId: string, event: string, data: any) {
        Logger.info(`Send to ${socketId} - event ${event} - ${JSON.stringify(data)}`)
        return Realtime.privateIO.to(socketId).emit(event, JSON.stringify(data))
    }

    public sendToUser(userId: string, event: string, data: any) {
        Logger.info(`Send to ${userId} - event ${event} - ${JSON.stringify(data)}`)
        return Realtime.privateIO.to(Convention.userRoom(userId)).emit(event, JSON.stringify(data))
    }

    public sendRawToUser(userId: string, event: string, dataRaw: any) {
        Logger.info(`Send to ${userId} - event ${event} - ${dataRaw}`)
        return Realtime.privateIO.to(Convention.userRoom(userId)).emit(event, dataRaw)
    }

    public sendToRoom(room: string, event: string, data: any) {
        Logger.info(`Send data to Room: ${room} - Event: ${event} - Data: ${JSON.stringify(data)}`)
        Logger.info(`Rooms have users ${JSON.stringify(Realtime.privateIO.adapter.rooms[room])}`)
        return Realtime.privateIO.to(room).emit(event, JSON.stringify(data))
    }

    public joinToRoom(userId: string, socketId: string, room: string) {
        const socket = Realtime.privateIO.sockets[socketId]
        if (!socket) return null
        if (socket.user.id !== userId) return null
        Logger.info(`Socket Private of ${userId} join to ${room}`)
        socket.join(room)
        return socket
    }

    public leaveRoom(userId: string, socketId: string, room: string) {
        const socket = Realtime.privateIO.sockets[socketId]
        if (!socket) return null
        if (socket.user.id !== userId) return null
        socket.leave(room)
        return socket
    }

    public getUserBySocketId(socketId: string) {
        const socket = Realtime.privateIO.sockets[socketId]
        if (!socket) return null
        return socket.user
    }

    public handleWhenDisconnected(rooms: string[], user: any) {
        rooms.forEach((room) => {
            ConversationManager.removeUserOfRoomConversation(room, user)
            this.sendToRoom(room, userAction[USER.OFFLINE].socket, user)
        })
    }

    public getSocketInRooms(room: string): any[] {
        const result = []
        const rooms = Realtime.privateIO.adapter.rooms[room]
        if (!rooms) return []
        for (let socketId in rooms.sockets) {
            if (rooms.sockets[socketId]) {
                result.push(Realtime.privateIO.connected[socketId])
            }
        }
        return result
    }

    public sendListUsersToRoom(room: string, event: string) {
        const users = this.getSocketInRooms(room).map((socket) => socket.user)
        this.sendToRoom(room, event, this.uniqueListUser(users))
    }

    private uniqueListUser(users: any[]) {
        const uniqueUsers: any  = {}
        users.forEach((user) => uniqueUsers[user.id] = user)
        return Object.keys(uniqueUsers).map((key: string) => uniqueUsers[key])
    }

    private getListRoomsFromSocket(socket: any, exclude: string[]) {
        let listRooms = []
        for (let k in socket.rooms) { // Get list rooms
            if (exclude.indexOf(socket.rooms[k]) < 0) listRooms.push(socket.rooms[k])
        }
        return listRooms
    }

    private listen() {
        Realtime.privateIO.on('connection', (socket: any) => {
            Logger.info(`CONNECTED userId: ${socket.user.id}, socketId: ${socket.id}`)
            socket.join(Convention.userRoom(socket.user.id)) // Join inner room by id.

            socket.on('blob', (data: any) => {
                Logger.info(`Have new data Blob from client ${socket.user.id}`)
                Redis.get(socket.user.id).then((toUserId) => {
                    Logger.info(`Send data Blob to ${toUserId}`)
                    if (toUserId) this.sendRawToUser(toUserId, 'call', data)
                })
            })
            
            socket.on('disconnecting', () => {
                const userId: string = socket.user ? socket.user.id : ''
                const listRooms = this.getListRoomsFromSocket(socket, [socket.id, Convention.userRoom(userId)])
                Logger.info(`Handle leave all rooms join ${listRooms}`)
                this.handleWhenDisconnected(listRooms, socket.user)
            })

            socket.on('disconnect', (data: any) => {
                Logger.info(`DISCONNECTED ${socket.id}`)
            })
        })

        server.listen(this.port, () => {
            Logger.info(`====REALTIME SERVER RUNNING PORT ${this.port} ====`)
        })
    }

    private validateAuthenticate(socket: any, next: () => void) {
        Logger.info(`Have new connected from, ${socket.id}`)
        const token = socket.handshake.query.token
        const tokenType = socket.handshake.query.tokenType || 'Bearer'
        if (!token) return Error('Missing token')
        request.get(`${process.env.REST_SERVER}/oauth/token`, {
            headers: {
                'Authorization': `${tokenType} ${token}`
            }
        }, (error: any, response: any, body: any) => {
            if (error) return Error('Token is not correct')
            if (response && response.statusCode > 300) return Error('Can not validate token')
            body = SafeParseJSON(body)
            Logger.info(`Logger with user ${body.user.id}`)
            if (body.user) {
                socket.user = body.user
                next()
            }
        })
    }
}
