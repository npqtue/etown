import {IPub, ISub} from '../service/Config'
import Realtime from '../Realtime'
import {
    Convention,
    conversationAction,
    userAction,
    CONVERSATION,
    USER,
} from '../service/Constants'
import logger from '../service/Logger'
import {SafeParseJSON} from '../service/StringUtil'
import Redis from '../service/RedisBase'
import ConversationManager from '../service/ConversationRedis'

class ConversationEvent {

    private pub: IPub
    private sub: ISub
    private io: Realtime

    constructor(pub: IPub, sub: ISub, io: Realtime) {
        this.pub = pub
        this.sub = sub
        this.io = io
        this.listen()
    }

    public joinConversation() {
        const channelJoin = conversationAction[CONVERSATION.JOIN]
        const channelOnline = userAction[USER.ONLINE]
        this.sub(channelJoin.nats, (msg: string) => {
            logger.info(`${channelJoin.socket}:  ${msg}`)
            const data = SafeParseJSON(msg)
            const socket = this.io.joinToRoom(data.user_id, data.socket_id, Convention.conversationRoom(data.conversation_id))
            if (socket) { // If Join room success
                const user = this.io.getUserBySocketId(socket.id)
                ConversationManager.addNewUserIntoConversation(data.conversation_id, user)
                this.io.sendToSocket(socket.id, channelJoin.socket, data)
                this.io.sendToRoom(
                    Convention.conversationRoom(data.conversation_id),
                    channelOnline.socket,
                    user
                )
            }
        })
    }

    public leaveConversation() {
        const channelLeave = conversationAction[CONVERSATION.LEAVE]
        const channelOffline = userAction[USER.OFFLINE]
        this.sub(channelLeave.nats, (msg: string) => {
            logger.info(`${channelLeave.socket}: ${msg}`)
            const data = SafeParseJSON(msg)
            const socket = this.io.leaveRoom(data.user_id, data.socket_id, Convention.conversationRoom(data.conversation_id))
            if (socket) { // If leave success
                const user = this.io.getUserBySocketId(socket.id)
                ConversationManager.removeUserOfConversation(data.conversation_id, user)
                this.io.sendToSocket(socket.id, channelLeave.socket, data)
                this.io.sendToRoom(
                    Convention.conversationRoom(data.conversation_id),
                    channelOffline.socket,
                    user
                )
            }
        })
    }

    public sendMessageToConversation() {
        const channelChat = conversationAction[CONVERSATION.NEW_CHAT]
        this.sub(channelChat.nats, (msg: string) => {
            logger.info(`${channelChat.socket}:  ${msg}`)
            const data = SafeParseJSON(msg)
            this.io.sendToRoom(Convention.conversationRoom(data.conversation_id), channelChat.socket, {
                message: data.message,
                action: data.action
            })
        })
    }

    public newRaiseHand() {
        const channelRaiseHand = conversationAction[CONVERSATION.NEW_RAISE_HAND]
        this.sub(channelRaiseHand.nats, (msg: string) => {
            logger.info(`${channelRaiseHand.socket}: ${msg}`)
            const data = SafeParseJSON(msg)
            ConversationManager.getUserByUserId(data.conversation_id, data.user_id).then((user: any) => {
                this.io.sendToRoom(
                    Convention.conversationRoom(data.conversation_id),
                    channelRaiseHand.socket,
                    user
                )
            })
        })
    }

    public acceptCall() {
        const channelAcceptCall = conversationAction[CONVERSATION.ACCEPT_CALL]
        this.sub(channelAcceptCall.nats, (msg: string) => {
            logger.info(`${channelAcceptCall.socket}: ${msg}`)
            const data = SafeParseJSON(msg)
            this.io.sendToUser(data.instructor_id, channelAcceptCall.socket, data)
            this.io.sendToUser(data.user_id, channelAcceptCall.socket, data)
        })
    }

    public closeCall() {
        const closeCallChannel = conversationAction[CONVERSATION.CLOSE_CALL]
        this.sub(closeCallChannel.nats, (msg: string) => {
            logger.info(`${closeCallChannel.socket}: ${msg}`)
            const data = SafeParseJSON(msg)
            this.io.sendToUser(data.instructor_id, closeCallChannel.socket, data)
            this.io.sendToUser(data.user_id, closeCallChannel.socket, data)
        })
    }

    public sendDirectMessage() {
        const channelDirectMessage = conversationAction[CONVERSATION.DIRECT_MESSAGE]
        this.sub(channelDirectMessage.nats, (msg: string) => {
            logger.info(`${channelDirectMessage.socket}: ${msg}`)
            const data = SafeParseJSON(msg)
            this.io.sendToUser(data.user_id, channelDirectMessage.socket, data)
        })
    }

    private listen() {
        this.joinConversation()
        this.sendMessageToConversation()
        this.leaveConversation()
        this.acceptCall()
        this.newRaiseHand()
        this.sendDirectMessage()
        this.closeCall()
    }
}

export default ConversationEvent
