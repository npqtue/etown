import { IPub, ISub} from '../service/Config'
import Realtime from '../Realtime'
import logger from '../service/Logger'
import {SafeParseJSON} from '../service/StringUtil'
import {LIVESTREAM, livestreamAction} from '../service/Constants'

class LiveStreamEvent {
    private pub: IPub
    private sub: ISub
    private io: Realtime

    constructor(pub: IPub, sub: ISub, io: Realtime) {
        this.pub = pub
        this.sub = sub
        this.io = io
        this.listen()
    }

    public newPublishData() {
        const channelLiveStreamPublish = livestreamAction[LIVESTREAM.NEW_PUBLISH]
        this.sub(channelLiveStreamPublish.nats, (msg: string) => {
            logger.info(`${channelLiveStreamPublish.socket}:  ${msg}`)
            const data = SafeParseJSON(msg)
            setTimeout(() => {
                this.io.sendToUser(data.instructor_id, channelLiveStreamPublish.socket, 'Received data')
            }, Number(process.env.DELAY_PUBLISH || 15) * 1000)
        })
    }

    private listen() {
        this.newPublishData()
    }
}

export default LiveStreamEvent
