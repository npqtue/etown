
require('dotenv').config()
import Realtime from './Realtime'
import NATS, {pub, sub} from './service/Nats'
import LivestreamEvent from './events/LivestreamEvent'
import ConversationEvent from './events/ConversationEvent'
import Initializer from './Initializer'
import Logger from './service/Logger'

Initializer().then((msg: string) => {
    Logger.info('Init Success')
    const realtime = new Realtime()
    // Register subscriber
    const _liveStreamEvent = new LivestreamEvent(pub, sub, realtime)
    const _conversationEvent = new ConversationEvent(pub, sub, realtime)
})
