export interface ISub {
    (channel: string, cb: (msg: string) => void): void
}

export interface IPub {
    (channel: string, data: any, cb: (data: any) => void): void
}
