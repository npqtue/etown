export interface Action {
    nats: string
    socket: string
}

export interface GroupAction {
    [_: string]: Action
}

// Todo: Event of conversation
export const CONVERSATION = {
    NEW_CHAT: 'newChat',
    JOIN: 'join',
    LEAVE: 'leave',
    ACCEPT_CALL: 'acceptCall',
    NEW_RAISE_HAND: 'raiseHand',
    DIRECT_MESSAGE: 'directMessage',
    CLOSE_CALL: 'closeCall'
}

export const conversationAction: GroupAction = {
    newChat : {
        nats: 'conversations.message',
        socket: 'conversations:message'
    },
    join: {
        nats: 'conversations.join',
        socket: 'conversations:join'
    },
    leave: {
        nats: 'conversations.leave',
        socket: 'conversations:leave'
    },
    acceptCall: {
        nats: 'livestream.acceptCall',
        socket: 'livestream:acceptCall'
    },
    closeCall: {
        nats: 'livestream.closeCall',
        socket: 'livestream:closeCall'
    },
    raiseHand: {
        nats: 'conversations.raiseHand',
        socket: 'conversations:raiseHand'
    },
    directMessage: {
        nats: 'users.message',
        socket: 'users:message'
    }
}


// Todo: Event Live Stream
export const LIVESTREAM = {
    NEW_PUBLISH: 'newPublish',
    ACCEPT_CALL: 'acceptCall'
}

export const livestreamAction: GroupAction = {
    newPublish: {
        nats: 'publish.new',
        socket: 'livestream:publish'
    }
}

export const USER = {
    ONLINE: 'online',
    OFFLINE: 'offline'
}

export const userAction: GroupAction = {
    online: {
        nats: '....!',
        socket: 'users:online'
    },
    offline: {
        nats: '....!',
        socket: 'users:offline'
    }
}

export const Convention = {
    conversationRoom: (conversationId: string) => {
        return `conversations:${conversationId}`
    },
    userRoom: (userId: string) => {
        return `users:${userId}`
    }
}
