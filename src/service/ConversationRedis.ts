import {RedisBase} from './RedisBase'
import {Convention} from './Constants'

class ConversationRedisManager {
    private client: RedisBase

    constructor(host: string, port: number, dbConversation: number) {
        this.client = new RedisBase(host, port, dbConversation)
    }

    public initializer() {
        return this.client.removeDataInstance()
    }

    public addNewUserIntoConversation(conversationId: string, user: any) {
        const userPromise = this.getUserByUserId(conversationId, user.id)
        userPromise.then((userFromRedis: any) => {
            if (!userFromRedis) {
                this.client.set(`${Convention.conversationRoom(conversationId)}:${user.id}`, user)
            }
        })
    }

    public removeUserOfConversation(conversationId: string, user: any) {
        return this.client.remove(`${Convention.conversationRoom(conversationId)}:${user.id}`)
    }

    public removeUserOfRoomConversation(roomConversation: string, user: any) {
        return this.client.remove(`${roomConversation}:${user.id}`)
    }

    public getUserByUserId(conversationId: string, userId: string) {
        return this.client.get(`${Convention.conversationRoom(conversationId)}:${userId}`)
    }
}

export default new ConversationRedisManager(
    process.env.REDIS_HOST || 'redis',
    Number(process.env.REDIS_PORT) || 6379,
    Number(process.env.REDIS_DB_ROOM_MANAGE) || 0
)
