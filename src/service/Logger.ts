const winston = require('winston')
const Sentry = require('winston-raven-sentry')
const { combine, timestamp, label, printf } = winston.format

const etownFormat = printf((info: any) => {
    return `${info.timestamp} [${info.label}] ${info.level}: ${info.message}`
})

const options = {
    dsn: process.env.DSN,
    level: 'error'
}

const logger = winston.createLogger({
    format: combine(
        label({'label': 'Etown Realtime'}),
        timestamp(),
        etownFormat
    ),
    transports: [
        new (winston.transports.Console)({'timestamp': true}),
        new Sentry(options)
    ]
})

export default logger
