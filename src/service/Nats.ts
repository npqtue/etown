import * as NATS from 'nats'
import {IPub, ISub} from './Config'

const nats = NATS.connect({
    url: process.env.NATS_SERVER,
    user: process.env.NATS_USER,
    pass: process.env.NATS_PASSWORD
})

export const sub: ISub = (channel: string, cb: Function) => {
    nats.subscribe(channel, cb)
}

export const pub: IPub = (channel: string, data: any, cb?: any) => {
    nats.publish(channel, JSON.stringify(data), cb)
}

export default {nats, pub, sub}
