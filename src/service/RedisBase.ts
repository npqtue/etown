import * as redis from 'ioredis'
import {SafeParseJSON} from './StringUtil'

export class RedisBase {
    private redis: redis.Redis

    constructor(host: string = 'redis', port: number = 6379, db: number = 4) {
        this.redis = redis({host: host, port: port, db: db})
    }

    public set(key: string, value: any) {
        this.redis.set(key, JSON.stringify(value))
    }

    public get(key: string) {
        return this.redis.get(key).then((msg) => {
            if (msg) return SafeParseJSON(msg)
            return null
        })
    }

    public remove(key: string) {
        return this.redis.del(key)
    }

    public getAllKeyByPrefix(prefix: string = '') {
        return this.redis.keys(prefix).then((keys: string[]) => {
            return keys
        })
    }

    public removeDataInstance() {
        return this.redis.flushdb()
    }
}

export default new RedisBase(process.env.REDIS_HOST, Number(process.env.REDIS_PORT))
