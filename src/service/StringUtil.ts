import Logger from './Logger'

export function SafeParseJSON(msg: string): any {
    const newMSG = msg.replace(/\\n/g, "\\n")
               .replace(/\\'/g, "\\'")
               .replace(/\\"/g, '\\"')
               .replace(/\\&/g, "\\&")
               .replace(/\\r/g, "\\r")
               .replace(/\\t/g, "\\t")
               .replace(/\\b/g, "\\b")
               .replace(/\\f/g, "\\f")
    let result = {}
    try {
        result = JSON.parse(newMSG)
    } catch {
        Logger.error('Can not parse msg to json')
    }
    return result
}
